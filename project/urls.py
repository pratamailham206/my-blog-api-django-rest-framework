from django.urls import path

from .views import GameList, GameDetail, WebsiteList, WebsiteDetail

app_name = 'project'

urlpatterns = [
    path('game/', GameList.as_view(), name='game_list'),
    path('game/<str:slug>/', GameDetail.as_view(), name='game_detail'),
    path('website/', WebsiteList.as_view(), name='website_list'),
    path('website/<str:slug>/', WebsiteDetail.as_view(), name='website_detail')
]