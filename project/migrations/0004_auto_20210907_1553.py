# Generated by Django 3.2.6 on 2021-09-07 08:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0003_auto_20210905_1718'),
    ]

    operations = [
        migrations.AddField(
            model_name='game',
            name='href',
            field=models.URLField(max_length=250, null=True),
        ),
        migrations.AddField(
            model_name='website',
            name='href',
            field=models.URLField(max_length=250, null=True),
        ),
    ]
