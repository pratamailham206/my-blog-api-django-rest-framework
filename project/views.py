from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Game, Website
from .serializers import GameSerializer, WebsiteSerializer
from django.http import Http404


class GameList(APIView):
    """
    List all games
    """
    def get(self, request, format=None):
        games = Game.objects.all()
        serializer = GameSerializer(games, context={ "request": request }, many=True)
        return Response(serializer.data)


class GameDetail(APIView):
    """
    Retrieve Game data
    """
    def get_object(self, slug):
        try:
            return Game.objects.get(slug=slug)
        except Game.DoesNotExist:
            raise Http404
    
    def get(self, request, slug, format=None):
        game = self.get_object(slug)
        serializer = GameSerializer(game, context={ "request": request })
        return Response(serializer.data)


class WebsiteList(APIView):
    """
    List all games
    """
    def get(self, request, format=None):
        website = Website.objects.all()
        serializer = WebsiteSerializer(website, context={ "request": request }, many=True)
        return Response(serializer.data)


class WebsiteDetail(APIView):
    """
    Retrieve Website Data
    """
    """
    Retrieve Game data
    """
    def get_object(self, slug):
        try:
            return Website.objects.get(slug=slug)
        except Website.DoesNotExist:
            raise Http404
    
    def get(self, request, slug, format=None):
        website = self.get_object(slug)
        serializer = WebsiteSerializer(website, context={ "request": request })
        return Response(serializer.data)