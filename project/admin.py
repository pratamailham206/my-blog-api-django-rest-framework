from django.contrib import admin

from .models import Game, GameImage, Website, WebsiteImage


@admin.register(Game)
class GameAdmin(admin.ModelAdmin):
    list_display = ('title', 'id', 'desc', 'content', 'published', 'slug', 'author')
    prepopulated_fields = {'slug': ('title',), }


@admin.register(Website)
class WebsiteAdmin(admin.ModelAdmin):
    list_display = ('title', 'id', 'desc', 'content', 'published', 'slug', 'author')
    prepopulated_fields = {'slug': ('title',), }


admin.site.register(GameImage)
admin.site.register(WebsiteImage)
