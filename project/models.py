from django.conf import settings
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from blog.models import Category


def website_upload_to(instance, filename):
    return 'website/{filename}'.format(filename=filename)


def game_upload_to(instance, filename):
    return 'games/{filename}'.format(filename=filename)


class Game(models.Model):
    category = models.ForeignKey(Category, on_delete=models.PROTECT, default=1)
    title = models.CharField(max_length=255)
    desc = models.CharField(max_length=500)
    content = models.TextField()
    published = models.DateTimeField(default=timezone.now)
    slug = models.SlugField(unique=True, max_length=255, unique_for_date='published')
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='game_project')
    href = models.URLField(max_length=250, null=True)
    objects = models.Manager()

    class Meta:
        ordering = ('-published',)

    def __str__(self):
        return self.title


class GameImage(models.Model):
    game = models.ForeignKey(Game, on_delete=models.CASCADE, related_name='game_image')
    image = models.ImageField(
        _("Image"), upload_to=game_upload_to, default='games/default.png')


class Website(models.Model):
    category = models.ForeignKey(Category, on_delete=models.PROTECT, default=1)
    title = models.CharField(max_length=255)
    desc = models.CharField(max_length=500)
    content = models.TextField()
    published = models.DateTimeField(default=timezone.now)
    slug = models.SlugField(unique=True, max_length=255, unique_for_date='published')
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='website_project')
    href = models.URLField(max_length=250, null=True)
    objects = models.Manager()

    class Meta:
        ordering = ('-published',)

    def __str__(self):
        return self.title


class WebsiteImage(models.Model):
    website = models.ForeignKey(Website, on_delete=models.CASCADE, related_name='website_image')
    image = models.ImageField(
        _("Image"), upload_to=website_upload_to, default='website/default.png')
