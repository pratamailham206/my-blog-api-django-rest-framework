from rest_framework import serializers

from .models import Game, GameImage, Website, WebsiteImage


class GameImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = GameImage
        fields = ('id', 'image')


class GameSerializer(serializers.ModelSerializer):
    game_image = GameImageSerializer(many=True, read_only=True)
    category_name = serializers.CharField(source='category.name')

    class Meta:
        model = Game
        fields = ('id', 'category_name', 'title', 'desc', 'content', 'published', 'slug', 'author', 'game_image', 'href')


class WebsiteImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = WebsiteImage
        fields = ('id', 'image')


class WebsiteSerializer(serializers.ModelSerializer):
    website_image = WebsiteImageSerializer(many=True, read_only=True)
    category_name = serializers.CharField(source='category.name')

    class Meta:
        model = Website
        fields = ('id', 'category_name', 'title', 'desc', 'content', 'published', 'slug', 'author', 'website_image', 'href')