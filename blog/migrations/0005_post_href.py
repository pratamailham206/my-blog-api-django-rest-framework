# Generated by Django 3.2.6 on 2021-09-07 08:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0004_alter_post_category'),
    ]

    operations = [
        migrations.AddField(
            model_name='post',
            name='href',
            field=models.URLField(max_length=250, null=True),
        ),
    ]
