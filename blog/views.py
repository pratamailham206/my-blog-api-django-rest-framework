from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Post
from .serializers import PostSerializer


class Postslist(APIView):
    """
    List all posts
    """
    def get(self, request, format=None):
        posts = Post.objects.all()
        serializer = PostSerializer(posts, context={"request": request}, many=True)
        return Response(serializer.data)
