from django.urls import path
from django.views.generic import TemplateView

from .views import Postslist

app_name = 'blog'

urlpatterns = [
    path('', TemplateView.as_view(template_name="index.html")),
    path('api/posts/', Postslist.as_view(), name='post_list')
]
