from rest_framework import serializers

from .models import Post


class PostSerializer(serializers.ModelSerializer):
    category_name = serializers.CharField(source='category.name')

    class Meta:
        model = Post
        fields = ('id', 'category_name', 'title', 'image', 'content', 'slug', 'duration', 'published', 'author', 'status', 'href')
